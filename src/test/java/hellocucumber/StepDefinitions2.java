package hellocucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StepDefinitions2 {
    String actual;


    @Given("today is Sunday")
    public void today_is_sunday() {

    }
    @When("I ask whether it's Friday yet")
    public void i_ask_whether_it_s_friday_yet() {
        actual="actual";
    }
    @Then("I should be told {string}")
    public void i_should_be_told(String expected) {
        // Write code here that turns the phrase above into concrete actions
        assertEquals(expected,actual);
    }

}
